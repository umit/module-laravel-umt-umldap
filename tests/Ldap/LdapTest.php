<?php

/**

 *
 * @author Nick Shontz <nick.shontz@umontana.edu>
 */
require(dirname(dirname(dirname(__FILE__))) . '/src/Ldap.php');

class LdapTest extends \PHPUnit_Framework_TestCase {

    function __construct() {
        parent::__construct();
        include('config.php');
        $this->EXAMPLE_USER = $example_user;
        $this->EXAMPLE_USER_LAST_NAME = $example_user_last_name;
        $this->EXAMPLE_USER_UMID = $example_user_umid;

        $this->ldap = new ldap($host, $user, $password, $base_dn);
	    if($this->ldap->error) {
		    die($this->ldap->error);
	    }
    }

    function testGetUser() {
        $user = $this->ldap->get_user_by_netid($this->EXAMPLE_USER);
        $this->assertEquals($this->EXAMPLE_USER, $user->netid);
    }

    function testLastNameSearch() {
        $users = $this->ldap->last_name_search($this->EXAMPLE_USER_LAST_NAME);
        $this->assertEquals(strtolower($users[0]->last_name),strtolower($this->EXAMPLE_USER_LAST_NAME));
    }

    function testGetUserByUmid() {
        $users = $this->ldap->get_user_by_umid($this->EXAMPLE_USER_UMID);
        $this->assertEquals(strtolower($users->umid),strtolower($this->EXAMPLE_USER_UMID));
    }

	function testGetUserInfo() {
		$users = $this->ldap->get_user_info($this->EXAMPLE_USER_UMID);
		$this->assertEquals(strtolower($users->umid),strtolower($this->EXAMPLE_USER_UMID));
	}
	function testSearchByAttributes() {
		$users = $this->ldap->search_by_attributes($this->EXAMPLE_USER_UMID);
		$this->assertEquals(strtolower($users->umid),strtolower($this->EXAMPLE_USER_UMID));
	}
}