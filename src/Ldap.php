<?php

/**
 * @author Nick Shontz <nick.shontz@umontana.edu>
 * @package         umldap
 * @subpackage      Libraries
 */
require_once 'Person.php';

class Ldap
{

    private $connection;
    private $basedn;
    private $host;
    private $user;
    private $password;
    public $attributes;
    public $error = false;

    function __construct($host, $user, $password, $basedn)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->basedn = $basedn;
        $this->attributes = array('cn', 'sn', 'givenname', 'uid', 'umid', 'activitycode', 'mail');
        $this->connect($host, $user, $password);
    }

    function __destruct()
    {
        if ($this->connection) {
            ldap_unbind($this->connection);
        }
    }

    function last_name_search($last_name)
    {
        return $this->search_by_attribute('sn', $last_name);
    }

    function get_user_by_netid($netid, $require_active = true)
    {
        return $this->find_by_attribute('uid', $netid, $require_active);
    }

    function get_user_info($id)
    {
        return $this->get_user_by_umid($id);
    }

    function get_user_by_umid($id)
    {
        return $this->find_by_attribute('umid', $id);
    }

    public function clear_session()
    {
        foreach ($_SESSION as $key => $value) {
            if (is_numeric(stripos($key, "um_ldap_"))) {
                unset($_SESSION[$key]);
            }
        }
    }

    public function search_by_attributes($values, $require_active = true)
    {
        $query = "";
        foreach ($values as $key => $value) {
            $query .= "({$key}={$value})";
        }
        $query = "(&" . $query . ($require_active ? "(activitycode=A)" : "") . ")";
        return $this->query($query);
    }


    private function find_by_attribute($key, $value, $require_active = true)
    {
        $results = $this->search_by_attributes(array($key => $value), $require_active);

        return count($results) > 0 ? $results[0] : null;
    }

    private function search_by_attribute($key, $value, $require_active = true)
    {
        return $this->search_by_attributes(array($key => $value), $require_active);

    }

    private function query($query)
    {
        $this->connect($this->host, $this->user, $this->password);
        if ($this->connection != null) {
            $result = ldap_search($this->connection, $this->basedn, $query, $this->attributes);
            $entries = null;
            $users = array();
            if ($result != false) {
                $entries = ldap_get_entries($this->connection, $result);
                foreach ($entries as $entry) {
                    if (!empty($entry['uid'][0])) {
                        $users[$entry['uid'][0]] = $this->convert_entry_to_person($entry);
                    }
                }
                sort($users);
            }
        } else {
            $users = array();
        }
        $this->disconnect();

        return $users;
    }

    private function disconnect()
    {
        if ($this->connection) {
            ldap_unbind($this->connection);
            $this->connection = null;
        }
    }

    private function connect($ldap_host, $ldap_user, $ldap_password)
    {
        $this->connection = ldap_connect("ldap://{$ldap_host}");
        ldap_set_option($this->connection, LDAP_OPT_TIMELIMIT, 20);
        ldap_set_option($this->connection, LDAP_OPT_NETWORK_TIMEOUT, 20);
        ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->connection, LDAP_OPT_REFERRALS, 0);

        $result = @ldap_bind($this->connection, $ldap_user, $ldap_password);
        if (!$result) {
            $this->error = ldap_error($this->connection);
            $this->connection = null;
        }
    }

    private function convert_entry_to_person($entry)
    {
        return new person($entry['uid'][0], $entry['cn'][0], $entry['sn'][0], $entry['givenname'][0], $entry['umid'][0], isset($entry['activitycode']) ? $entry['activitycode'][0] : "", isset($entry['mail']) ? $entry['mail'][0] : "");
    }

}